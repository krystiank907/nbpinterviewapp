# NBPInterviewApp


## Requirements

### Before Run 
1. Go to project folder in terminal
2. Run `pod install` 
3. Open `NbpInterviewApp.xcworkspace`

## Description of App
1. On the top we heave header with base information like  `title of table`,  `number od table`, `segment control for chcange table`
2. On mian view you can pull down to refresh data
3. After select any currency. you will see a detail of currency with chart
4. On the top of view we have navigation bar with `Tile`,  `Back button` and  `reload button`
5. Under it we have header view with some simple information and two button for pickup a start and end date 
6. On the chart view you can scroll left and right to see more data (if available)

## Author

Krystian Kulawiak, iApps4U 

