//
//  NBPApplicationCoordinator.swift
//  nbpInterviewApp
//
//  Created by Krystian Kulawiak on 13/01/2021.
//

import UIKit
import RxSwift
import RxCocoa

class NBPApplicationCoordinator: NBPBaseCoordinator<Void> {
    
    override func start() {
        showMain()
    }
    
    private func showMain() {
        let mainFlowCoordinator = NBPMainCoordinator(navigationController: self.navigationController)
        self.run(coordinator: mainFlowCoordinator)
        mainFlowCoordinator.finishFlowHandler = { [weak self] value in
            self?.freeAll()
        }
    }
    
}
