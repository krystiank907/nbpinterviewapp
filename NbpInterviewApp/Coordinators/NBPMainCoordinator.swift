//
//  NBPMainCoordinator.swift
//  NbpInterviewApp
//
//  Created by Krystian Kulawiak on 13/01/2021.
//

import UIKit

class NBPMainCoordinator: NBPBaseCoordinator<Void> {
    
    private let container = NBPDependencyInjection.createChild()
    
    override func start() {
        setupMain()
    }
    
    private func setupMain() {
        guard let mainController = container.resolve(NBPMainViewController.self) else { return }
        mainController.selectionHandler = { [weak self] (values) in
            self?.showDetailCurrency(tableType: values.tableType, code: values.code)
        }
        mainController.errorHandler = defaultErrorHandler
        navigationController.viewControllers = [mainController]
    }
    
    private func showDetailCurrency(tableType: NBPCurrencyTableTypes, code: String) {
        guard let detailController = container.resolve(NBPDetailViewController.self, arguments: tableType, code) else { return }
        detailController.selectionHandler = { [weak self] dataPicker in
            self?.showDatePicker(dataPicker: dataPicker)
        }
        detailController.errorHandler = defaultErrorHandler
        let leftNavigationButtonHandler: NBPEmptyHandler = { [weak self] in
            self?.navigationController.popViewController(animated: true)
        }
        let rightNavigationButtonHandler: NBPEmptyHandler = { [weak detailController] in
            detailController?.getData()
        }
        let navigationConfig = CommonNavigationBarConfiguration(title: code,
                                                                leftNavigationButtonImage: R.image.arrowLeft(),
                                                                rightNavigationButtonImage: R.image.reloadIcon(),
                                                                leftNavigationButtonHandler: leftNavigationButtonHandler,
                                                                rightNavigationButtonHandler: rightNavigationButtonHandler)
        detailController.navigationItemConfiguration = navigationConfig
        navigationController.pushViewController(detailController, animated: true)
    }
    
    private func showDatePicker(dataPicker: NBPDatePickerData) {
        guard let datePickerPopup = container.resolve(NBPDatePickerPopup.self, argument: dataPicker) else { return }
        datePickerPopup.doneHandler = { [weak datePickerPopup] date in
            datePickerPopup?.viewModel.dateSelectionHandler?(date)
            datePickerPopup?.dismiss(animated: true, completion: nil)
        }
        datePickerPopup.modalPresentationStyle = .overCurrentContext
        navigationController.present(datePickerPopup, animated: true, completion: nil)
    }
}
