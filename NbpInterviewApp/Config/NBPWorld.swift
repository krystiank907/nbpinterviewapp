//
//  NBPWorld.swift
//  NbpInterviewApp
//
//  Created by Krystian Kulawiak on 13/01/2021.
//

import Foundation
import Alamofire

protocol NBPWorldScheme {
    static var shared: NBPWorldScheme { get }

    var apiClient: NBPClientService { get }
    var apiClientConfiguration: NBPClientConfiguration { get }

}

struct NBPWorld {
    let apiClient: NBPClientService
    let apiClientConfiguration: NBPClientConfiguration

    private init(apiClient: NBPClientService, apiClientConfiguration: NBPClientConfiguration) {
        self.apiClient = apiClient
        self.apiClientConfiguration = apiClientConfiguration
        self.apiClient.setConfiguration(apiClientConfiguration)
    }
}

extension NBPWorld: NBPWorldScheme {
    
    static var shared: NBPWorldScheme = {
        let baseUrl = "http://api.nbp.pl/api/exchangerates/"
        return NBPWorld(apiClient: APIClientService.shared,
                        apiClientConfiguration: APIClientConfiguration(baseUrl: baseUrl))
    }()
    
}
