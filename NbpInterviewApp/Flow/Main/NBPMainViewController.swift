//
//  NBPMainViewController.swift
//  NbpInterviewApp
//
//  Created by Krystian Kulawiak on 13/01/2021.
//

import UIKit

class NBPMainViewController: NBPViewController<VMMainScheme> {
    private let scrollView: UIScrollView = UIScrollView()
    private let headerView: NBPMainHeaderContainerView = NBPMainHeaderContainerView()
    private let tableView: UITableView = UITableView()
    private let refreshControl: UIRefreshControl = UIRefreshControl()
    
    var selectionHandler: NBPValueHandler<(tableType: NBPCurrencyTableTypes, code: String)> = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupViews()
        setupTableView()
        makeConstraints()
        getData(tableTitle: viewModel.segmentsTitles.first ?? "")
    }
    
    private func loadViewsData() {
        headerView.noText = viewModel.noText
        headerView.title = viewModel.title
        tableView.reloadData()
    }
    
    private func setupViews() {
        viewModel.activityIndicatorHandler = { [weak self] value in
            self?.handleByDefaultActivityIndicator(value)
        }
        viewModel.setupActivityService()
        headerView.segments = viewModel.segmentsTitles
        headerView.segmentDidChange = { [weak self] tableTitle in
            self?.getData(tableTitle: tableTitle)
            self?.tableView.setContentOffset(.zero, animated: false)
        }
        headerView.selectedSegmentIndex = 0
    }
    
    private func setupTableView() {
        tableView.delegate = self
        tableView.dataSource = self
        tableView.separatorStyle = .none
        tableView.register(NBPMainTableViewCell.self, forCellReuseIdentifier: NBPMainTableViewCell.standardIdentifier)
        tableView.addSubview(refreshControl)
        refreshControl.addTarget(self, action: #selector(refreshAction), for: .valueChanged)
    }
    
    @objc private func refreshAction() {
        getData(tableTitle: viewModel.currentSelectedTable.rawValue) { [weak self] in
            self?.refreshControl.endRefreshing()
        }
    }
    
    private func getData(tableTitle: String, _ completion: NBPEmptyHandler = nil) {
        viewModel.getData(tableTitle: tableTitle) { [weak self] (result) in
            switch result {
            case .failure(let error):
                self?.errorHandler?.throw(error)
                completion?()
            case .success:
                self?.loadViewsData()
                completion?()
            }
        }
    }
    
    private func makeConstraints() {
        view.addSubview(headerView)
        view.addSubview(tableView)
        headerView.snp.makeConstraints { (make) in
            make.leading.equalToSuperview().offset(20)
            make.top.equalTo(view.snp.topMargin).offset(20)
            make.trailing.equalToSuperview().offset(-20)
        }
        tableView.snp.makeConstraints { (make) in
            make.top.equalTo(headerView.snp.bottom).offset(15)
            make.leading.trailing.bottom.equalToSuperview()
        }
    }
    
}

extension NBPMainViewController: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.numberOfRows
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: NBPMainTableViewCell.standardIdentifier) as? NBPMainTableViewCell,
              let data = viewModel.cellData(for: indexPath) else { return UITableViewCell() }
        cell.setupCell(data: data)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let data = viewModel.cellData(for: indexPath) else { return }
        selectionHandler?((viewModel.currentSelectedTable, data.code))
    }
    
}
