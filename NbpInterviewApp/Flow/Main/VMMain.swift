//
//  VMMain.swift
//  NbpInterviewApp
//
//  Created by Krystian Kulawiak on 13/01/2021.
//

import Foundation


protocol VMMainScheme: NBPNetworkViewModelScheme {
    var title: String? { get }
    var noText: String? { get }
    var numberOfRows: Int { get }
    var currentSelectedTable: NBPCurrencyTableTypes { get }
    var segmentsTitles: [String] { get }
    var activityService: NetworkActivityServiceScheme? { get set }
    var activityIndicatorHandler: NBPValueHandler<Bool> { get set }
    
    func getData(tableTitle: String, completion: @escaping ResultCompletion<Void>)
    func cellData(for indexPath: IndexPath) -> NBPMainTableViewCellData?
}

class VMMain: VMMainScheme {
    private var apiData: NBPCurrencyTableDTO?
    
    var title: String? {
        return "loc_main_title_\(currentSelectedTable)".localized
    }
    var noText: String? {
        return apiData?.no
    }
    var segmentsTitles: [String] {
        return NBPCurrencyTableTypes.allCases.compactMap { $0.rawValue }
    }
    var numberOfRows: Int {
        return apiData?.rates.count ?? 0
    }
    var activityService: NetworkActivityServiceScheme?
    var activityIndicatorHandler: NBPValueHandler<Bool> = nil
    var currentSelectedTable: NBPCurrencyTableTypes = .tableA
    
    func getData(tableTitle: String, completion: @escaping ResultCompletion<Void>) {
        guard let tableType = NBPCurrencyTableTypes(rawValue: tableTitle) else {
            completion(.failure(APIClientError.commonError))
            return
        }
        currentSelectedTable = tableType
        apiClient.request(router: NBPCurrencyRouter.getAllTable(table: tableType), activityService: activityService) { [weak self] (result: Result<NBPCurrencyTableResponse>) in
            switch result {
            case .failure(let error):
                completion(.failure(error))
            case .success(let response):
                guard let data = response.data?.first else {
                    completion(.failure(APIClientError.commonError))
                    return
                }
                self?.apiData = data
                completion(.success(()))
            }
        }
    }
    
    func cellData(for indexPath: IndexPath) -> NBPMainTableViewCellData? {
        guard let apiRatesData = apiData?.rates[safe: indexPath.row] else { return nil }
        let value: Double = apiRatesData.mid ?? apiRatesData.bid ?? 0
        return .init(date: apiData?.effectiveDate,
                     name: apiRatesData.currency,
                     code: apiRatesData.code,
                     value: value,
                     extraValue: apiRatesData.ask)
    }
    
}
