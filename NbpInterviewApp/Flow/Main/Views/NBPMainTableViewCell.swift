//
//  NBPMainTableViewCell.swift
//  NbpInterviewApp
//
//  Created by Krystian Kulawiak on 13/01/2021.
//

import UIKit

struct NBPMainTableViewCellData {
    var date: Date?
    var name: String
    var code: String
    var value: Double
    var extraValue: Double?
}

class NBPMainTableViewCell: UITableViewCell {
    
    private var containerView: UIView = UIView()
    private var nameContainerView: UIView = UIView()
    
    private var dateLabel: UILabel = UILabel()
    private var nameCurrencyLabel: UILabel = UILabel()
    private var codeCurrencyLabel: UILabel = UILabel()
    private var valuesStackView: UIStackView = UIStackView()
    
    private var valueLabel: UILabel = UILabel()
    private var extraValueLabel: UILabel = UILabel()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupViews()
        makeConstriaints()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setupViews() {
        selectionStyle = .none
        containerView.layer.masksToBounds = true
        containerView.layer.cornerRadius = 10
        nameContainerView.backgroundColor = UIColor.nbpGreen.withAlphaComponent(0.7)
        containerView.backgroundColor = UIColor.lightGray.withAlphaComponent(0.5)
        codeCurrencyLabel.font = UIFont.boldSystemFont(ofSize: 18)
        nameCurrencyLabel.font = .systemFont(ofSize: 12, weight: .light)
        dateLabel.font = .systemFont(ofSize: 10, weight: .light)
        nameCurrencyLabel.numberOfLines = 0
        nameCurrencyLabel.lineBreakMode = .byWordWrapping
        nameCurrencyLabel.textAlignment = .center
        nameCurrencyLabel.textColor = .white
        valuesStackView.axis = .horizontal
        valuesStackView.alignment = .fill
        valuesStackView.distribution = .fillEqually
        valuesStackView.spacing = 5
    }
    
    private func makeConstriaints() {
        contentView.addSubview(containerView)
        containerView.addSubview(nameContainerView)
        nameContainerView.addSubview(nameCurrencyLabel)
        containerView.addSubview(codeCurrencyLabel)
        containerView.addSubview(valuesStackView)
        containerView.addSubview(dateLabel)
        
        containerView.snp.makeConstraints { (make) in
            make.edges.equalToSuperview().inset(UIEdgeInsets(top: 0, left: 20, bottom: 15, right: 20))
        }
        nameContainerView.snp.makeConstraints { (make) in
            make.top.leading.trailing.equalToSuperview()
            make.height.equalTo(30)
        }
        codeCurrencyLabel.snp.makeConstraints { (make) in
            make.top.equalTo(nameContainerView.snp.bottom)
            make.bottom.equalToSuperview()
            make.width.equalTo(45)
            make.height.equalTo(60)
            make.leading.equalToSuperview().offset(10)
        }
        valuesStackView.snp.makeConstraints { (make) in
            make.top.equalTo(nameContainerView.snp.bottom)
            make.bottom.equalToSuperview()
            make.trailing.equalToSuperview().offset(-10)
        }
        nameCurrencyLabel.snp.makeConstraints { (make) in
            make.centerX.centerY.equalToSuperview()
        }
        dateLabel.snp.makeConstraints { (make) in
            make.top.equalTo(nameContainerView.snp.bottom)
            make.bottom.equalToSuperview()
            make.leading.equalTo(codeCurrencyLabel.snp.trailing).offset(15)
            make.trailing.lessThanOrEqualTo(valuesStackView.snp.leading).offset(-15)
        }
    }
    
    func setupCell(data: NBPMainTableViewCellData) {
        if let date = data.date {
            dateLabel.text = DateFormatter.nbpAPIFormatter.string(from: date)
        }
        valuesStackView.arrangedSubviews.forEach({ $0.removeFromSuperview() })
        valuesStackView.addArrangedSubview(valueLabel)
        valueLabel.text = "\(data.value.rounded(toPlaces: 4))"
        
        if let extraValue = data.extraValue {
            valuesStackView.addArrangedSubview(extraValueLabel)
            extraValueLabel.text = "\(extraValue.rounded(toPlaces: 4))"
        }
        codeCurrencyLabel.text = data.code
        nameCurrencyLabel.text = data.name
    }
    
}
