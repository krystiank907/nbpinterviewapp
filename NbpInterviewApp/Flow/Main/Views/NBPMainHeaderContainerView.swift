//
//  NBPMainHeaderContainerView.swift
//  NbpInterviewApp
//
//  Created by Krystian Kulawiak on 13/01/2021.
//

import UIKit
import RxSwift
import RxCocoa
import SnapKit

class NBPMainHeaderContainerView: UIView {
    
    private var containerView: UIView = UIView()
    private var titleLabel: UILabel = UILabel()
    private var noLabel: UILabel = UILabel()
    private var segmantControll: UISegmentedControl = UISegmentedControl()
    var segmentDidChange: NBPValueHandler<String> = nil
    var disposeBag = DisposeBag()
    var segments: [String] = [] {
        didSet {
            segmantControll.removeAllSegments()
            segments.enumerated().forEach { (index, segmentTitle) in
                segmantControll.insertSegment(withTitle: segmentTitle, at: index, animated: false)
            }
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupViews()
        makeConstraints()
        setupObserve()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    private func setupViews() {
        containerView.clipsToBounds = false
        containerView.layer.cornerRadius = 10
        containerView.backgroundColor = UIColor.nbpGreen
        titleLabel.textColor = .white
        titleLabel.numberOfLines = 0
        noLabel.textColor = .white
        segmantControll.tintColor = .white
    }
    
    private func makeConstraints() {
        addSubview(containerView)
        containerView.addSubview(titleLabel)
        containerView.addSubview(noLabel)
        containerView.addSubview(segmantControll)
        containerView.snp.makeConstraints { (make) in
            make.edges.equalToSuperview()
        }
        titleLabel.snp.makeConstraints { (make) in
            make.leading.top.equalToSuperview().offset(15)
            make.trailing.equalToSuperview().offset(-15)
        }
        noLabel.snp.makeConstraints { (make) in
            make.top.equalTo(titleLabel.snp.bottom).offset(10)
            make.leading.equalToSuperview().offset(15)
            make.trailing.equalToSuperview().offset(-15)
        }
        segmantControll.snp.makeConstraints { (make) in
            make.top.equalTo(noLabel.snp.bottom).offset(10)
            make.leading.equalToSuperview().offset(15)
            make.trailing.bottom.equalToSuperview().offset(-15)
        }
    }
    
    private func setupObserve() {
        segmantControll.rx.selectedSegmentIndex.skip(1).subscribe(onNext: { [weak self] index in
            guard let titleForIndex = self?.segmantControll.titleForSegment(at: index) else { return }
            self?.segmentDidChange?(titleForIndex)
        }).disposed(by: disposeBag)
    }
    
}

extension NBPMainHeaderContainerView {
    
    var selectedSegmentIndex: Int {
        get {
            return segmantControll.selectedSegmentIndex
        } set {
            segmantControll.selectedSegmentIndex = newValue
        }
    }
    
    var title: String? {
        get {
            return titleLabel.text
        } set {
            titleLabel.text = newValue
        }
    }
    
    var noText: String? {
        get {
            return noLabel.text
        } set {
            noLabel.text = newValue
        }
    }
    
}
