//
//  VMDatePicker.swift
//  NbpInterviewApp
//
//  Created by Krystian Kulawiak on 13/01/2021.
//

import Foundation

struct NBPDatePickerData {
    var title: String
    var minimumDate: Date
    var maximumDate: Date
    var currentSelectedDate: Date
    var dateSelectionHandler: NBPValueHandler<Date>
}

protocol VMDatePickerScheme {
    var title: String { get }
    var minimumDate: Date { get }
    var maximumDate: Date { get }
    var currentSelectedDate: Date { get }
    var dateSelectionHandler: NBPValueHandler<Date> { get }
    
    init(initData: NBPDatePickerData)
}

class VMDatePicker: VMDatePickerScheme {
    
    var title: String
    var minimumDate: Date
    var maximumDate: Date
    var currentSelectedDate: Date
    var dateSelectionHandler: NBPValueHandler<Date>
    
    required init(initData: NBPDatePickerData) {
        self.title = initData.title
        self.minimumDate = initData.minimumDate
        self.maximumDate = initData.maximumDate
        self.currentSelectedDate = initData.currentSelectedDate
        self.dateSelectionHandler = initData.dateSelectionHandler
    }
    
}
