//
//  NBPDatePickerPopup.swift
//  NbpInterviewApp
//
//  Created by Krystian Kulawiak on 13/01/2021.
//

import UIKit
import RxSwift
import RxCocoa

class NBPDatePickerPopup: NBPViewController<VMDatePickerScheme> {
    
    private var containerView: UIView = UIView()
    private var headerContainerView: UIView = UIView()
    private var titleLabel: UILabel = UILabel()
    private var doneButton: UIButton = UIButton()
    private var datePicker: UIDatePicker = UIDatePicker()
    private var disposeBag = DisposeBag()
    var doneHandler: NBPValueHandler<Date> = nil 
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupViews()
        makeConstraints()
    }
    
    private func setupViews() {
        view.backgroundColor = UIColor.black.withAlphaComponent(0.6)
        containerView.clipsToBounds = true
        containerView.layer.masksToBounds = true
        containerView.layer.cornerRadius = 10
        containerView.backgroundColor = .white
        headerContainerView.backgroundColor = .nbpGreen
        titleLabel.text = viewModel.title
        titleLabel.textColor = .white
        datePicker.datePickerMode = .date
        if #available(iOS 13.4, *) {
            datePicker.preferredDatePickerStyle = .wheels
        }
        datePicker.minimumDate = viewModel.minimumDate
        datePicker.maximumDate = viewModel.maximumDate
        datePicker.date = viewModel.currentSelectedDate
        datePicker.calendar = Calendar(identifier: .iso8601)
        datePicker.timeZone = TimeZone(secondsFromGMT: 0)
        datePicker.locale = Locale(identifier: "en_US_POSIX")
        doneButton.setTitle("loc_done".localized, for: .normal)
        doneButton.rx.tap.subscribe(onNext: { [weak self] in
            guard let self = self else { return }
            self.doneHandler?(self.datePicker.date)
        }).disposed(by: disposeBag)
    }
    
    private func makeConstraints() {
        view.addSubview(containerView)
        containerView.addSubview(headerContainerView)
        headerContainerView.addSubview(titleLabel)
        headerContainerView.addSubview(doneButton)
        containerView.addSubview(datePicker)
        
        containerView.snp.makeConstraints { (make) in
            make.leading.equalToSuperview().offset(30)
            make.trailing.equalToSuperview().offset(-30)
            make.centerY.equalToSuperview()
        }
        headerContainerView.snp.makeConstraints { (make) in
            make.leading.top.trailing.equalToSuperview()
            make.height.equalTo(50)
        }
        titleLabel.snp.makeConstraints { (make) in
            make.centerX.centerY.equalToSuperview()
        }
        doneButton.snp.makeConstraints { (make) in
            make.trailing.equalToSuperview().offset(-15)
            make.centerY.equalToSuperview()
        }
        datePicker.snp.makeConstraints { (make) in
            make.leading.trailing.bottom.equalToSuperview()
            make.top.equalTo(headerContainerView.snp.bottom).offset(10)
        }
    }
    
}
