//
//  NBPDetailViewController.swift
//  NbpInterviewApp
//
//  Created by Krystian Kulawiak on 13/01/2021.
//

import UIKit
import Charts

class NBPDetailViewController: NBPViewController<VMDetailScheme> {
    
    override var prefersNavigationBarHidden: Bool {
        return false
    }
    
    private let headerView: NBPDetailHeaderView = NBPDetailHeaderView()
    private let scrollView: UIScrollView = UIScrollView()
    private let scrollContentView: UIView = UIView()
    private let chartView: LineChartView = LineChartView()
    var selectionHandler: NBPValueHandler<NBPDatePickerData> = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupViews()
        makeConstraints()
        getData()
    }
    
    private func loadViewsData() {
        headerView.codeText = viewModel.codeText
        headerView.nameText = viewModel.nameText
        headerView.tableText = viewModel.tableText
        headerView.startDateTitle = viewModel.startDateTitle
        headerView.endDateTitle = viewModel.endDateTitle
        headerView.didSelectButton = { [weak self] buttonType in
            guard let self = self else { return }
            self.selectionHandler?(self.viewModel.getDataPickerDate(type: buttonType))
        }
    }
    
    private func loadChartData() {
        scrollContentView.snp.remakeConstraints { (make) in
            make.edges.equalToSuperview()
            let maxWidth = max(viewModel.chartLabelsXaxisCount * 80, Int(scrollView.frame.width)) + 10
            make.width.equalTo(maxWidth)
            make.height.equalToSuperview().priority(.low)
        }
        chartView.xAxis.setLabelCount(viewModel.chartLabelsXaxisCount, force: true)
        chartView.xAxis.valueFormatter = ChartXAxisFormatter(dateObjects: viewModel.chartFormatterDateObjects)
        chartView.data = LineChartData(dataSets: viewModel.chartDataSet)
    }
    
    private func setupViews() {
        viewModel.activityIndicatorHandler = { [weak self] value in
            self?.handleByDefaultActivityIndicator(value)
        }
        viewModel.fetchNewData = { [weak self] in
            self?.getData()
        }
        viewModel.setupActivityService()
        
        chartView.dragEnabled = false
        chartView.setScaleEnabled(false)
        chartView.pinchZoomEnabled = false
        chartView.legend.enabled = true
        chartView.rightAxis.enabled = false
        chartView.legend.form = .line
        chartView.setExtraOffsets(left: 10, top: 20, right: 30, bottom: 20)
        
        let xAxis = chartView.xAxis
        xAxis.labelPosition = .top
        xAxis.labelFont = .systemFont(ofSize: 10, weight: .light)
        xAxis.labelTextColor = .nbpGreen
        xAxis.drawAxisLineEnabled = true
        xAxis.drawGridLinesEnabled = true
        xAxis.centerAxisLabelsEnabled = false
        xAxis.granularity = 1
        
        let leftAxis = chartView.leftAxis
        leftAxis.labelPosition = .outsideChart
        leftAxis.labelFont = .systemFont(ofSize: 10, weight: .light)
        leftAxis.drawGridLinesEnabled = false
        leftAxis.granularityEnabled = false
        leftAxis.labelTextColor = .nbpGreen
    }
    
    func getData(_ completion: NBPEmptyHandler = nil) {
        viewModel.getData { [weak self] (result) in
            switch result {
            case .failure(let error):
                self?.errorHandler?.throw(error)
                completion?()
            case .success:
                self?.loadViewsData()
                self?.loadChartData()
                completion?()
            }
        }
    }
    
    private func makeConstraints() {
        view.addSubview(headerView)
        view.addSubview(scrollView)
        scrollView.addSubview(scrollContentView)
        scrollContentView.addSubview(chartView)
        headerView.snp.makeConstraints { (make) in
            make.leading.equalToSuperview().offset(20)
            make.top.equalTo(view.snp.topMargin).offset(20)
            make.trailing.equalToSuperview().offset(-20)
        }
        scrollView.snp.makeConstraints { (make) in
            make.top.equalTo(headerView.snp.bottom).offset(15)
            make.leading.bottom.trailing.equalToSuperview()
        }
        chartView.snp.makeConstraints { (make) in
            make.leading.equalToSuperview().offset(20)
            make.trailing.equalToSuperview().offset(-20)
            make.top.equalToSuperview()
            make.height.equalToSuperview().multipliedBy(0.9)
        }
    }
    
}


