//
//  VMDetail.swift
//  NbpInterviewApp
//
//  Created by Krystian Kulawiak on 13/01/2021.
//

import Foundation
import Charts

protocol VMDetailScheme: NBPNetworkViewModelScheme {
    var codeText: String? { get }
    var nameText: String? { get }
    var tableText: String? { get }
    var startDateTitle: String? { get }
    var endDateTitle: String? { get }
    var chartDataSet: [LineChartDataSet] { get }
    var chartLabelsXaxisCount: Int { get }
    var startDate: Date { get set }
    var chartFormatterDateObjects: [Date] { get }
    var endDate: Date { get set }
    var activityService: NetworkActivityServiceScheme? { get set }
    var activityIndicatorHandler: NBPValueHandler<Bool> { get set }
    var fetchNewData: NBPEmptyHandler { get set }
    
    func getData(completion: @escaping ResultCompletion<Void>)
    func getDataPickerDate(type: NBPDetailHeaderButtonType) -> NBPDatePickerData
    
    init(tableType: NBPCurrencyTableTypes, currencyCode: String)
}

class VMDetail: VMDetailScheme {

    private var apiData: NBPCurrencyRateDTO?
    private var tableType: NBPCurrencyTableTypes
    private var currencyCode: String
    
    var nameText: String? {
        return apiData?.currency
    }
    var tableText: String? {
        return "loc_table_titile".localizedWithFormat(arguments: "\(apiData?.table ?? "-")")
    }
    var startDateTitle: String? {
        return DateFormatter.nbpAPIFormatter.string(from: startDate)
    }
    var endDateTitle: String? {
        return DateFormatter.nbpAPIFormatter.string(from: endDate)
    }
    var codeText: String? {
        return apiData?.code
    }
    var chartLabelsXaxisCount: Int {
        return apiData?.rates.count ?? 0
    }
    var chartDataSet: [LineChartDataSet] {
        switch tableType {
        case .tableA, .tableB:
            let entry = apiData?.rates.enumerated().compactMap({ (index, data) -> ChartDataEntry? in
                return .init(x: Double(index), y: data.mid ?? 0)
            })
            return [LineChartDataSet(entries: entry, label: "loc_detail_chart_title_avg".localized)]
        case .tableC:
            let entryASK = apiData?.rates.enumerated().compactMap({ (index, data) -> ChartDataEntry? in
                return .init(x: Double(index), y: data.ask ?? 0)
            })
            let entryBID = apiData?.rates.enumerated().compactMap({ (index, data) -> ChartDataEntry? in
                return .init(x: Double(index), y: data.bid ?? 0)
            })
            let askChartDataSet = LineChartDataSet(entries: entryASK, label: "loc_detail_chart_title_sell".localized)
            askChartDataSet.setColor(.magenta)
            askChartDataSet.setCircleColor(.magenta)
            return [askChartDataSet, LineChartDataSet(entries: entryBID, label: "loc_detail_chart_title_buy".localized)]
        }
    }
    var chartFormatterDateObjects: [Date] {
        return apiData?.rates.compactMap({ $0.effectiveDate }) ?? []
    }
    var fetchNewData: NBPEmptyHandler = nil
    var startDate: Date
    var endDate: Date
    var activityService: NetworkActivityServiceScheme?
    var activityIndicatorHandler: NBPValueHandler<Bool> = nil
    
    required init(tableType: NBPCurrencyTableTypes, currencyCode: String) {
        self.tableType = tableType
        self.currencyCode = currencyCode
        self.endDate = Date()
        self.startDate = Calendar.current.date(byAdding: .day, value: -30, to: Date()) ?? Date()
    }
    
    func getData(completion: @escaping ResultCompletion<Void>) {
        let startDate = DateFormatter.nbpAPIFormatter.string(from: self.startDate)
        let endDate = DateFormatter.nbpAPIFormatter.string(from: self.endDate)
        let router: Router = NBPCurrencyRouter.getRate(table: tableType,
                                                       code: currencyCode,
                                                       startDate: startDate,
                                                       endDate: endDate)
        apiClient.request(router: router, activityService: activityService) { [weak self] (result: Result<NBPCurrencyRateResponse>) in
            switch result {
            case .failure(let error):
                completion(.failure(error))
            case .success(let response):
                guard let data = response.data else {
                    completion(.failure(APIClientError.commonError))
                    return
                }
                self?.apiData = data
                completion(.success(()))
            }
        }
    }
    
    func getDataPickerDate(type: NBPDetailHeaderButtonType) -> NBPDatePickerData {
        switch type {
        case .startDate:
            let minimumDate = Calendar.current.date(byAdding: .day, value: -93, to: Date()) ?? Date()
            let handler: NBPValueHandler<Date> = { [weak self] startDate in
                self?.startDate = startDate
                self?.fetchNewData?()
            }
            return NBPDatePickerData(title: "loc_detail_startDate".localized,
                                     minimumDate: minimumDate,
                                     maximumDate: Date(),
                                     currentSelectedDate: startDate,
                                     dateSelectionHandler: handler)
        case .endDate:
            let minimumDate = Calendar.current.date(byAdding: .day, value: -93, to: Date()) ?? Date()
            let handler: NBPValueHandler<Date> = { [weak self] endDate in
                self?.endDate = endDate
                self?.fetchNewData?()
            }
            return NBPDatePickerData(title: "loc_detail_endDate".localized,
                                     minimumDate: minimumDate,
                                     maximumDate: Date(),
                                     currentSelectedDate: endDate,
                                     dateSelectionHandler: handler)
        }
    }
    
}

