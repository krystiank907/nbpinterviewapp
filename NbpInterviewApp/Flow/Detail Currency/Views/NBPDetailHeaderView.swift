//
//  NBPDetailHeaderView.swift
//  NbpInterviewApp
//
//  Created by Krystian Kulawiak on 13/01/2021.
//

import UIKit
import RxSwift
import RxCocoa
import SnapKit

enum NBPDetailHeaderButtonType {
    case startDate
    case endDate
}

class NBPDetailHeaderView: UIView {
    
    private var containerView: UIView = UIView()
    private var codeLabel: UILabel = UILabel()
    private var nameLabel: UILabel = UILabel()
    private var tableLabel: UILabel = UILabel()
    private var buttonsStackView: UIStackView = UIStackView()
    
    private var startDateButton: UIButton = UIButton()
    private var endDateButton: UIButton = UIButton()
    
    var didSelectButton: NBPValueHandler<NBPDetailHeaderButtonType> = nil
    var disposeBag = DisposeBag()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupViews()
        makeConstraints()
        setupObserve()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    private func setupViews() {
        containerView.clipsToBounds = false
        containerView.layer.cornerRadius = 10
        containerView.backgroundColor = UIColor.nbpGreen
        codeLabel.textColor = .white
        codeLabel.font = UIFont.boldSystemFont(ofSize: 22)
        nameLabel.textColor = .white
        nameLabel.numberOfLines = 0
        tableLabel.textColor = .white
        buttonsStackView.axis = .horizontal
        buttonsStackView.alignment = .fill
        buttonsStackView.distribution = .fillEqually
        buttonsStackView.spacing = 15
        [startDateButton, endDateButton].forEach {
            $0.clipsToBounds = false
            $0.layer.cornerRadius = 6
            $0.backgroundColor = UIColor.white.withAlphaComponent(0.9)
            $0.setTitleColor(.nbpGreen, for: .normal)
            $0.setTitleColor(UIColor.nbpGreen.withAlphaComponent(0.4), for: .highlighted)
        }
    }
    
    private func makeConstraints() {
        addSubview(containerView)
        containerView.addSubview(codeLabel)
        containerView.addSubview(nameLabel)
        containerView.addSubview(tableLabel)
        containerView.addSubview(buttonsStackView)
        containerView.snp.makeConstraints { (make) in
            make.edges.equalToSuperview()
        }
        codeLabel.snp.makeConstraints { (make) in
            make.leading.top.equalToSuperview().offset(15)
        }
        nameLabel.snp.makeConstraints { (make) in
            make.top.greaterThanOrEqualToSuperview().offset(15)
            make.bottom.equalTo(codeLabel.snp.bottom)
            make.leading.equalTo(codeLabel.snp.trailing).offset(15)
            make.trailing.equalToSuperview().offset(-15)
        }
        tableLabel.snp.makeConstraints { (make) in
            make.top.equalTo(codeLabel.snp.bottom).offset(10)
            make.leading.equalToSuperview().offset(15)
            make.trailing.equalToSuperview().offset(-15)
        }
        buttonsStackView.snp.makeConstraints { (make) in
            make.top.equalTo(tableLabel.snp.bottom).offset(10)
            make.leading.equalToSuperview().offset(15)
            make.trailing.bottom.equalToSuperview().offset(-15)
            make.height.equalTo(40)
        }
        buttonsStackView.addArrangedSubview(startDateButton)
        buttonsStackView.addArrangedSubview(endDateButton)
    }
    
    private func setupObserve() {
        startDateButton.rx.tap.subscribe(onNext: { [weak self] in
            self?.didSelectButton?(.startDate)
        }).disposed(by: disposeBag)
        
        endDateButton.rx.tap.subscribe(onNext: { [weak self] in
            self?.didSelectButton?(.endDate)
        }).disposed(by: disposeBag)
    }
    
}

extension NBPDetailHeaderView {
    
    var codeText: String? {
        get {
            return codeLabel.text
        } set {
            codeLabel.text = newValue
        }
    }
    
    var nameText: String? {
        get {
            return nameLabel.text
        } set {
            nameLabel.text = newValue
        }
    }
    
    var tableText: String? {
        get {
            return tableLabel.text
        } set {
            tableLabel.text = newValue
        }
    }
    
    var startDateTitle: String? {
        get {
            return startDateButton.title(for: .normal)
        } set {
            startDateButton.setTitle(newValue, for: .normal)
        }
    }
    
    var endDateTitle: String? {
        get {
            return endDateButton.title(for: .normal)
        } set {
            endDateButton.setTitle(newValue, for: .normal)
        }
    }
    
}



