//
//  ChartXAxisFormatter.swift
//  NbpInterviewApp
//
//  Created by Krystian Kulawiak on 13/01/2021.
//

import Foundation
import Charts

class ChartXAxisFormatter: NSObject {
    var dateObjects: [Date]
    var dateFormatter: DateFormatter = DateFormatter.nbpAPIFormatter
    
    init(dateObjects: [Date]) {
        self.dateObjects = dateObjects
    }
}

extension ChartXAxisFormatter: IAxisValueFormatter {

    func stringForValue(_ value: Double, axis: AxisBase?) -> String {
        let date = dateObjects[safe: Int(value)] ?? Date()
        return dateFormatter.string(from: date)
    }

}
