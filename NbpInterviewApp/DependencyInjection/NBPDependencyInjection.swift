//
//  NBPDependencyInjection.swift
//  nbpInterviewApp
//
//  Created by Krystian Kulawiak on 13/01/2021.
//

import Foundation
import Swinject

class NBPDependencyInjection {
    
    let container = Container()
    static let shared: NBPDependencyInjection = NBPDependencyInjection()
    
    class func createChild() -> Container {
        return Container(parent: shared.container)
    }
    
    init() {
        setupMainFlow()
        setupDetailFlow()
    }
    
    private func setupMainFlow() {
        container.register(VMMainScheme.self) { _ in VMMain() }
        container.register(NBPMainViewController.self) { res -> NBPMainViewController in
            let viewController = NBPMainViewController()
            viewController.viewModel = res.resolve(VMMainScheme.self)
            return viewController
        }
    }
    
    private func setupDetailFlow() {
        container.register(VMDetailScheme.self) { (_, arg1: NBPCurrencyTableTypes, arg2: String) -> VMDetailScheme in
            return VMDetail(tableType: arg1, currencyCode: arg2)
        }
        container.register(NBPDetailViewController.self) { (res, arg1: NBPCurrencyTableTypes, arg2: String) -> NBPDetailViewController in
            let viewController = NBPDetailViewController()
            viewController.viewModel = res.resolve(VMDetailScheme.self, arguments: arg1, arg2)
            return viewController
        }
        container.register(VMDatePickerScheme.self) { (_, arg1: NBPDatePickerData) -> VMDatePickerScheme in
            return VMDatePicker(initData: arg1)
        }
        container.register(NBPDatePickerPopup.self) { (res, arg1: NBPDatePickerData) -> NBPDatePickerPopup in
            let viewController = NBPDatePickerPopup()
            viewController.viewModel = res.resolve(VMDatePickerScheme.self, argument: arg1)
            return viewController
        }
    }
    
    
}

