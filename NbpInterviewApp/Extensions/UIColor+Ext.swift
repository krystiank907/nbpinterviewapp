//
//  UIColor+Ext.swift
//  NbpInterviewApp
//
//  Created by Krystian Kulawiak on 13/01/2021.
//

import UIKit

extension UIColor {
    
    static var nbpGreen: UIColor {
        return UIColor(red: 0.04, green: 0.35, blue: 0.30, alpha: 1.00)
    }
    
}
