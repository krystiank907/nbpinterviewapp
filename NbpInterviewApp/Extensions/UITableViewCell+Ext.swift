//
//  UITableViewCell+Ext.swift
//  NbpInterviewApp
//
//  Created by Krystian Kulawiak on 13/01/2021.
//

import UIKit

extension UITableViewCell {
    static var standardIdentifier: String {
        return String(describing: self)
    }
}

