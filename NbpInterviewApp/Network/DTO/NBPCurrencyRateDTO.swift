//
//  NBPCurrencyRateDTO.swift
//  NbpInterviewApp
//
//  Created by Krystian Kulawiak on 13/01/2021.
//

import Foundation

struct NBPCurrencyRateDTO: Codable {

    let table: String
    let currency: String
    let code: String
    let rates: [NBPCurrencyRateRatesDTO]
    
    init(table: String, currency: String, code: String, rates: [NBPCurrencyRateRatesDTO]) {
        self.table = table
        self.currency = currency
        self.code = code
        self.rates = rates
    }
    
    public var tableType: NBPCurrencyTableTypes {
        return NBPCurrencyTableTypes(rawValue: table) ?? .tableA
    }
}

struct NBPCurrencyRateRatesDTO: Codable {

    let no: String
    let effectiveDate: Date?
    let mid: Double?
    let bid: Double?
    let ask: Double?
    
    init(no: String, effectiveDate: Date?, mid: Double?, bid: Double?, ask: Double?) {
        self.no = no
        self.effectiveDate = effectiveDate
        self.mid = mid
        self.bid = bid
        self.ask = ask
    }
        
}

