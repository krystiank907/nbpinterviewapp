//
//  NBPCurrencyTableDTO.swift
//  NbpInterviewApp
//
//  Created by Krystian Kulawiak on 13/01/2021.
//

import Foundation

struct NBPCurrencyTableDTO: Codable {
    
    let table: String
    let no: String
    let effectiveDate: Date?
    let tradingDate: Date?
    let rates: [NBPCurrencyTableRatesDTO]
    
    init(table: String, no: String, effectiveDate: Date?, tradingDate: Date?, rates: [NBPCurrencyTableRatesDTO]) {
        self.table = table
        self.no = no
        self.effectiveDate = effectiveDate
        self.tradingDate = tradingDate
        self.rates = rates
    }
    
    public var tableType: NBPCurrencyTableTypes {
        return NBPCurrencyTableTypes(rawValue: table) ?? .tableA
    }
}

struct NBPCurrencyTableRatesDTO: Codable {
    
    let currency: String
    let code: String
    let mid: Double?
    let bid: Double?
    let ask: Double?
    
    init(currency: String, code: String, mid: Double?, bid: Double?, ask: Double?) {
        self.currency = currency
        self.code = code
        self.mid = mid
        self.bid = bid
        self.ask = ask
    }
        
}
