//
//  NBPCurrencyRouter.swift
//  NbpInterviewApp
//
//  Created by Krystian Kulawiak on 13/01/2021.
//

import Foundation
import Alamofire

enum NBPCurrencyTableTypes: String, CaseIterable {
    case tableA = "A"
    case tableB = "B"
    case tableC = "C"
}

enum NBPCurrencyRouter: Router {
    case getAllTable(table: NBPCurrencyTableTypes)
    case getRate(table: NBPCurrencyTableTypes, code: String, startDate: String, endDate: String)
    
    var path: String {
        switch self {
        case .getAllTable(let tableType):
            return "/tables/\(tableType.rawValue)/"
        case .getRate(let tableType, let code, let startDate, let endDate):
        return "/rates/\(tableType.rawValue)/\(code)/\(startDate)/\(endDate)"
        }
    }

    var method: HTTPMethod {
        switch self {
        case .getAllTable, .getRate:
            return .get
        }
    }

    var parameters: Parameters {
        return [:]
    }

    var headers: HTTPHeaders? {
        return nil
    }

}
