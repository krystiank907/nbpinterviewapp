//
//  NBPErrors.swift
//  NbpInterviewApp
//
//  Created by Krystian Kulawiak on 13/01/2021.
//

import Foundation

enum APIClientError: Error, Equatable {
    case networkError
    case commonError
    case customError(message: String)
}

