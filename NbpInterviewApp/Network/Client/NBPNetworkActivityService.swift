//
//  NBPNetworkActivityService.swift
//  NbpInterviewApp
//
//  Created by Krystian Kulawiak on 13/01/2021.
//

import Foundation

protocol NetworkActivityServiceScheme {
    init(dispatchGroup: DispatchGroup?, onActivityStart: NBPEmptyHandler, onActivityEnd: NBPEmptyHandler, onAllActivitesEnd: NBPEmptyHandler)

    func start()
    func stop()
    func notify()
}

class NetworkActivityService: NetworkActivityServiceScheme {
    private let onActivityStart: NBPEmptyHandler
    private let onActivityEnd: NBPEmptyHandler
    private let onAllActivitesEnd: NBPEmptyHandler
    private let dispatchGroup: DispatchGroup?

    required init(dispatchGroup: DispatchGroup? = DispatchGroup(),
                  onActivityStart: NBPEmptyHandler,
                  onActivityEnd: NBPEmptyHandler,
                  onAllActivitesEnd: NBPEmptyHandler = nil) {
        self.dispatchGroup = dispatchGroup
        self.onActivityStart = onActivityStart
        self.onActivityEnd = onActivityEnd
        self.onAllActivitesEnd = onAllActivitesEnd
    }

    func start() {
        dispatchGroup?.enter()
        onActivityStart?()
    }

    func stop() {
        dispatchGroup?.leave()
        onActivityEnd?()
    }

    func notify() {
        dispatchGroup?.notify(queue: .main, execute: { [weak self] in
            self?.onAllActivitesEnd?()
        })
    }
}
