//
//  NBPResponse.swift
//  NbpInterviewApp
//
//  Created by Krystian Kulawiak on 13/01/2021.
//

import Foundation
import Alamofire

private struct ResponseDestructiveKey {
    static var responseDictionaryData = "DestructiveKey.responseDictionaryData"
}

protocol Response {
    associatedtype CodableScheme: Codable

    var data: CodableScheme? { get set }
    var `internal`: HTTPURLResponse? { get set }
    var url: URL? { get set }
    var error: Error? { get }

    init()
    init(dictionaryData: [String: Any]?, data: CodableScheme?, internal: HTTPURLResponse?, url: URL?)
}

extension Response {
    
    var dictionaryData: [String: Any]? {
        get {
            return objc_getAssociatedObject(self, &ResponseDestructiveKey.responseDictionaryData) as? [String: Any]
        } set {
            objc_setAssociatedObject(self, &ResponseDestructiveKey.responseDictionaryData, newValue, .OBJC_ASSOCIATION_RETAIN_NONATOMIC)
        }
    }
    
    init(dictionaryData: [String: Any]?, data: CodableScheme?, internal: HTTPURLResponse?, url: URL?) {
        self.init()
        self.data = data
        self.internal = `internal`
        self.url = url
        self.dictionaryData = dictionaryData
    }

    var isSucceed: Bool {
        return (200..<300).contains(self.internal?.statusCode ?? 0)
    }
    
    var statusCode: Int {
        return self.internal?.statusCode ?? 0
    }
    
    var errorMessage: String? {
        return dictionaryData?["message"] as? String
    }
    
    var error: Error? {
        return baseError
    }
    
    var baseError: Error? {
        switch statusCode {
        case 400, 404:
            return APIClientError.commonError
        default:
            if data == nil {
                return APIClientError.commonError
            } else if let message = errorMessage, !isSucceed {
                return APIClientError.customError(message: message)
            } else if !isSucceed {
                return APIClientError.commonError
            } else {
                return nil
            }
        }
    }
}
