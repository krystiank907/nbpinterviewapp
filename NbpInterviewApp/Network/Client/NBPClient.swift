//
//  NBPClient.swift
//  NbpInterviewApp
//
//  Created by Krystian Kulawiak on 13/01/2021.
//

import Foundation
import Alamofire

protocol NBPClientConfiguration {
    var baseUrl: String { get set }
    var trustManager: ServerTrustManager? { get set }
}

struct APIClientConfiguration: NBPClientConfiguration {
    var baseUrl: String
    var trustManager: ServerTrustManager?
}

protocol NBPClientService {
    static var shared: NBPClientService { get }

    func request<U: Response>(router: Router, activityService: NetworkActivityServiceScheme?, result: @escaping ResultCompletion<U>)
    func setConfiguration(_ configuration: NBPClientConfiguration)
}

class APIClientService: NBPClientService {

    private var configuration: NBPClientConfiguration!

    private lazy var session: Alamofire.Session = {
        Alamofire.Session(configuration: APIClientService.configuration, serverTrustManager: configuration.trustManager)
    }()

    private static var configuration: URLSessionConfiguration {
        let configuration = URLSessionConfiguration.default
        configuration.timeoutIntervalForRequest = 120
        configuration.timeoutIntervalForResource = 120
        configuration.requestCachePolicy = .reloadIgnoringCacheData
        return configuration
    }

    static var shared: NBPClientService = APIClientService()


    func setConfiguration(_ configuration: NBPClientConfiguration) {
        self.configuration = configuration
    }


    func createEndpoint(_ request: Request) -> String {
        return configuration.baseUrl + request.endpointUrl
    }

    func request<U>(router: Router, activityService: NetworkActivityServiceScheme?, result: @escaping (Result<U>) -> Void) where U: Response {

        let request = router.request
        let method = router.method
        let url = createEndpoint(request)
        let parameters = request.parameters.isEmpty ? nil : request.parameters
        let headers = headersAdapter(request: request)

        DispatchQueue.main.async {
            activityService?.start()
        }

        session.request(url, method: method, parameters: parameters, encoding: Alamofire.JSONEncoding.default, headers: headers).response { (dataResponse) in
            let response = dataResponse.response
            let data = dataResponse.data

            if dataResponse.error != nil {
                DispatchQueue.main.async { result(.failure(APIClientError.networkError)) }
            } else if let data = data {
                var json: U.CodableScheme?
                do {
                    let decoder = JSONDecoder()
                    decoder.dateDecodingStrategy = .formatted(DateFormatter.nbpAPIFormatter)
                    json = try decoder.decode(U.CodableScheme.self, from: data)
                } catch {
                    if let errorDes = error as? DecodingError {
                        print(errorDes.errorDescription ?? "")
                    }
                }
                let dictionaryData = try? JSONSerialization.jsonObject(with: data, options: []) as? [String: Any]
                let parsedResponse: U = U(dictionaryData: dictionaryData, data: json, internal: response, url: URL(string: url))
                DispatchQueue.main.async {
                    if let error = parsedResponse.error {
                        result(.failure(error))
                    } else {
                        result(.success(parsedResponse))
                    }
                }
            }

            DispatchQueue.main.async {
                activityService?.stop()
            }
        }
    }
    

    private func headersAdapter(request: Request) -> HTTPHeaders {
        var customHeaders = HTTPHeaders()
        if let headers = request.headers {
            headers.forEach({customHeaders.update($0)})
        }
        customHeaders["Accept"] = "application/json"
        return customHeaders
    }

}

