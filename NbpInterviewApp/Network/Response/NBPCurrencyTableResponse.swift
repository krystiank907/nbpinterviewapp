//
//  NBPCurrencyTableResponse.swift
//  NbpInterviewApp
//
//  Created by Krystian Kulawiak on 13/01/2021.
//

import Foundation

struct NBPCurrencyTableResponse: Response {
    typealias CodableScheme = [NBPCurrencyTableDTO]

    var data: [NBPCurrencyTableDTO]?
    var `internal`: HTTPURLResponse?
    var url: URL?

    init() {
        
    }
    
}
