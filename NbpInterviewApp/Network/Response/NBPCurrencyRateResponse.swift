//
//  NBPCurrencyRateResponse.swift
//  NbpInterviewApp
//
//  Created by Krystian Kulawiak on 13/01/2021.
//

import Foundation

struct NBPCurrencyRateResponse: Response {
    typealias CodableScheme = NBPCurrencyRateDTO

    var data: NBPCurrencyRateDTO?
    var `internal`: HTTPURLResponse?
    var url: URL?

    init() {
        
    }
    
}
