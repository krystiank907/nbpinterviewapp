//
//  NBPBaseCoordinator.swift
//  nbpInterviewApp
//
//  Created by Krystian Kulawiak on 13/01/2021.
//

import UIKit
import RxSwift
import RxCocoa

class NBPBaseCoordinator<ResultType> {
    
    private let identifier = UUID()
    
    weak var navigationController: UINavigationController!
    var childCoordinators = [UUID: Any]()
    var finishFlowHandler: NBPValueHandler<ResultType?> = nil
    var disposeBag = DisposeBag()
    
    init(navigationController: UINavigationController) {
        self.navigationController = navigationController
    }
    
    func run<T>(coordinator: NBPBaseCoordinator<T>, triggerStart: Bool = true) {
        store(coordinator: coordinator)
        if triggerStart { coordinator.start() }
    }
    
    func start() {
        fatalError("It must be implemented in child class")
    }
    
    private func store<T>(coordinator: NBPBaseCoordinator<T>) {
        childCoordinators[coordinator.identifier] = coordinator
    }
    
    func free<T>(coordinator: NBPBaseCoordinator<T>) {
        childCoordinators[coordinator.identifier] = nil
    }
    
    func freeAll() {
        childCoordinators.removeAll()
    }
    
    var defaultErrorHandler: NBPErrorHandleable {
        let errorHandler = NBPErrorHandler()

        return errorHandler.catch(APIClientError.commonError, action: { [weak self] (_) in
            self?.showAlert(title: "loc_api_common_error".localized)
        }).catch(APIClientError.networkError, action: { [weak self] (_) in
            self?.showAlert(title: "loc_api_network_error".localized)
        }).listen(APIClientError.self) { [weak self] (error) in
            switch error {
            case .customError(let message):
                self?.showAlert(title: message)
            default:
                break
            }
        }
    }
    
    func showAlert(title: String, message: String? = nil) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let doneAction = UIAlertAction(title: "loc_ok".localized, style: .default) { [weak alert] (_) in
            alert?.dismiss(animated: true, completion: nil)
        }
        alert.addAction(doneAction)
        navigationController.present(alert, animated: true, completion: nil)
    }
    
}
