//
//  NBPNetworkViewModelScheme.swift
//  NbpInterviewApp
//
//  Created by Krystian Kulawiak on 13/01/2021.
//

import Foundation

protocol NBPNetworkViewModelScheme: class, NBPBaseViewModel {
    var apiClient: NBPClientService { get }
    var activityService: NetworkActivityServiceScheme? { get set }
    var activityIndicatorHandler: NBPValueHandler<Bool> { get set }
    func setupActivityService()
}

extension NBPNetworkViewModelScheme {
    var apiClient: NBPClientService {
        return NBPWorld.shared.apiClient
    }
}

extension NBPNetworkViewModelScheme {
    func setupActivityService() {
        self.activityService = NetworkActivityService(onActivityStart: { [weak self] in
            self?.activityIndicatorHandler?(true)
        }, onActivityEnd: { [weak self] in
            self?.activityIndicatorHandler?(false)
        })
    }
}

