//
//  NBPErrorHandleable.swift
//  NbpInterviewApp
//
//  Created by Krystian Kulawiak on 14/01/2021.
//

import Foundation

typealias HandleAction<T> = (T) throws -> Void

protocol NBPErrorHandleable: class {
    func `throw`(_: Error, finally: @escaping (Bool) -> Void)
    func `catch`(action: @escaping HandleAction<Error>) -> NBPErrorHandleable
}

class NBPErrorHandler: NBPErrorHandleable {
    private var parent: NBPErrorHandler?
    private let action: HandleAction<Error>

    convenience init(action: @escaping HandleAction<Error> = { throw $0 }) {
        self.init(action: action, parent: nil)
    }

    init(action: @escaping HandleAction<Error> = { throw $0 }, parent: NBPErrorHandler? = nil) {
        self.action = action
        self.parent = parent
    }

    func `throw`(_ error: Error, finally: @escaping (Bool) -> Void) {
        `throw`(error, previous: [], finally: finally)
    }

    private func `throw`(_ error: Error, previous: [NBPErrorHandler], finally: ((Bool) -> Void)? = nil) {
        if let parent = parent {
            parent.`throw`(error, previous: previous + [self], finally: finally)
            return
        }
        serve(error, next: AnyCollection(previous.reversed()), finally: finally)
    }

    private func serve(_ error: Error, next: AnyCollection<NBPErrorHandler>, finally: ((Bool) -> Void)? = nil) {
        do {
            try action(error)
            finally?(true)
        } catch {
            if let nextHandler = next.first {
                nextHandler.serve(error, next: next.dropFirst(), finally: finally)
            } else {
                finally?(false)
            }
        }
    }

    func `catch`(action: @escaping HandleAction<Error>) -> NBPErrorHandleable {
        return NBPErrorHandler(action: action, parent: self)
    }
}

extension NBPErrorHandleable {
    func `do`<A>(_ section: () throws -> A) {
        do {
            _ = try section()
        } catch {
            `throw`(error)
        }
    }
}

extension NBPErrorHandleable {
    func `throw`(_ error: Error) {
        `throw`(error, finally: { _ in })
    }
}

extension NBPErrorHandleable {
    func `catch`<Key: Error>(_ type: Key.Type, action: @escaping HandleAction<Key>) -> NBPErrorHandleable {
        return `catch`(action: { (value) in
            if let key = value as? Key {
                try action(key)
            } else {
                throw value
            }
        })
    }

    func `catch`<Key: Error>(_ value: Key, action: @escaping HandleAction<Key>) -> NBPErrorHandleable where Key: Equatable {
        return `catch`(action: { (item) in
            if let key = item as? Key, key == value {
                try action(key)
            } else {
                throw item
            }
        })
    }
}

extension NBPErrorHandleable {
    func listen(action: @escaping (Error) -> Void) -> NBPErrorHandleable {
        return `catch`(action: { value in
            action(value)
            throw value
        })
    }

    func listen<Key: Error>(_ type: Key.Type, action: @escaping (Key) -> Void) -> NBPErrorHandleable {
        return `catch`(type, action: { value in
            action(value)
            throw value
        })
    }

    func listen<Key: Error>(_ value: Key, action: @escaping (Key) -> Void) -> NBPErrorHandleable where Key: Equatable {
        return `catch`(value, action: { value in
            action(value)
            throw value
        })
    }
}
