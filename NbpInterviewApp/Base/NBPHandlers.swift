//
//  NBPHandlers.swift
//  nbpInterviewApp
//
//  Created by Krystian Kulawiak on 13/01/2021.
//

import Foundation

typealias NBPEmptyHandler = (() -> Void)?
typealias NBPValueHandler<T> = ((T) -> Void)?
typealias NBPOptionalValueHandler<T> = ((T?) -> Void)?
