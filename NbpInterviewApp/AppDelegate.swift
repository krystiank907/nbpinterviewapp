//
//  AppDelegate.swift
//  nbpInterviewApp
//
//  Created by Krystian Kulawiak on 12/01/2021.
//

import UIKit

@main
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    var applicationCoordinator: NBPApplicationCoordinator?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        #if DEBUG
        NetworkActivityLogger.shared.startLogging()
        #endif
        createRootViewController()
        return true
    }

    func createRootViewController() {
        if window == nil {
            window = UIWindow()
        }
        window?.makeKeyAndVisible()
        let rootVC = NBPBaseNavigationController()
        window?.rootViewController = rootVC
        applicationCoordinator = NBPApplicationCoordinator(navigationController: rootVC)
        applicationCoordinator?.start()
    }

}

