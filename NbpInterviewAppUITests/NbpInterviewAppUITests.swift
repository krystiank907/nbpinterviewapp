//
//  nbpInterviewAppUITests.swift
//  nbpInterviewAppUITests
//
//  Created by Krystian Kulawiak on 12/01/2021.
//

import XCTest

class NbpInterviewAppUITests: XCTestCase {
    
    let app = XCUIApplication()
    let bundle = Bundle(for: NbpInterviewAppUITests.self)

    override func setUpWithError() throws {
        continueAfterFailure = false
        app.launch()
    }

    func testMainFlow() throws {
        let tableAText = app.staticTexts["loc_main_title_tableA".localized(bundle: bundle)].waitForExistence(timeout: 5.0)
        XCTAssertTrue(tableAText)
        app.segmentedControls.buttons["B"].tap()
        let tableBText = app.staticTexts["loc_main_title_tableB".localized(bundle: bundle)].waitForExistence(timeout: 5.0)
        XCTAssertTrue(tableBText)
        app.segmentedControls.buttons["C"].tap()
        let tableCText = app.staticTexts["loc_main_title_tableC".localized(bundle: bundle)].waitForExistence(timeout: 5.0)
        XCTAssertTrue(tableCText)
    }

}
